import java.util.TreeSet;

class Book implements Comparable<Book> {
    private String title;
    private String author;
    private String ISBN;

    public Book(String title, String author, String ISBN) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
    }

    // Method untuk mendapatkan judul buku
    public String getTitle() {
        return title;
    }

    // Implementasi Method compareTo untuk urutan alami berdasarkan judul
    @Override
    public int compareTo(Book otherBook) {
        return this.title.compareTo(otherBook.getTitle());
    }

    // Method untuk mencetak informasi buku
    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", ISBN='" + ISBN + '\'' +
                '}';
    }
}

public class Task5 {
    public static void main(String[] args) {
        // Membuat objek TreeSet dari kelas Book
        TreeSet<Book> bookSet = new TreeSet<>();

        // Menambahkan buku ke dalam TreeSet
        bookSet.add(new Book("JKTPJAA ", "Alvi Syahrin", "123456789"));
        bookSet.add(new Book("JKTPJC", "Alvi Syahrin", "987654321"));
        bookSet.add(new Book("JKTPBBS", "Alvi Syahrin", "456123789"));


        // Mencetak hasil dalam urutan yang diurutkan
        System.out.println("TreeSet objek Book dalam urutan yang diurutkan:");
        for (Book book : bookSet) {
            System.out.println(book);
        }
    }
}

