import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Task1 {

    public static void main(String[] args) {
        List<String> regionList = new ArrayList<>();

        regionList.add("Jakarta");
        regionList.add("Bandung");
        regionList.add("Yogyakarta");
        regionList.add("Jakarta");

        System.out.println("Pre-Data : ");
        System.err.println(regionList);
        HashSet<String> setRegion = new HashSet<>(regionList);
        regionList.clear();
        regionList.addAll(setRegion);

        System.out.println("Post-Data : ");
        System.err.println(regionList);

        

    }
}