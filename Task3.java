class ListNode {
    int data;
    ListNode next;

    public ListNode(int data) {
        this.data = data;
        this.next = null;
    }
}

public class Task3 {
    private ListNode head;

    public Task3() {
        this.head = null;
    }

    // Method untuk menambahkan elemen ke tengah linked list
    public void insertInMiddle(int data) {
        if (head == null) {
            head = new ListNode(data);
        } else {
            ListNode newNode = new ListNode(data);
            ListNode slow = head;
            ListNode fast = head;

            while (fast != null && fast.next != null) {
                fast = fast.next.next;
                slow = slow.next;
            }

            newNode.next = slow.next;
            slow.next = newNode;
        }
    }

    // Method untuk mencetak linked list
    public void printList() {
        ListNode current = head;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Task3 integerList = new Task3();

        // Menambahkan elemen ke linked list
        integerList.insertInMiddle(1);
        integerList.insertInMiddle(2);
        integerList.insertInMiddle(4);
        integerList.insertInMiddle(5);

        // Mencetak linked list sebelum modifikasi
        System.out.println("Linked List Sebelum Modifikasi:");
        integerList.printList();

        // Menambahkan elemen ke tengah linked list
        integerList.insertInMiddle(3);

        // Mencetak linked list setelah modifikasi
        System.out.println("Linked List Setelah Modifikasi:");
        integerList.printList();
    }
}

