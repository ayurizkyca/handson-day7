import java.util.TreeSet;

public class Task4 {
    private final TreeSet<String> stringTreeSet = new TreeSet<>();

    // Method untuk menambahkan elemen ke TreeSet
    public void addToSet(String element) {
        stringTreeSet.add(element);
    }

    // Method untuk mencetak TreeSet setelah pengurutan
    public void printSortedSet() {
        System.out.println("TreeSet Setelah Pengurutan Alfabetis:");
        stringTreeSet.forEach(System.out::println);
    }

    public static void main(String[] args) {
        Task4 fruits = new Task4();
        
        // Menambahkan elemen ke TreeSet
        fruits.addToSet("Banana");
        fruits.addToSet("Apple");
        fruits.addToSet("Orange");
        fruits.addToSet("Grapes");
        fruits.addToSet("Pineapple");

        // Mencetak TreeSet setelah pengurutan
        fruits.printSortedSet();
    }
}
