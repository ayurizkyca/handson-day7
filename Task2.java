import java.util.HashMap;
import java.util.Scanner;

class Student {
    private String name, major, university;

    // Konstruktor
    public Student(String name, String major, String university) {
        this.name = name;
        this.major = major;
        this.university = university;
    }

    // Getter untuk masing-masing field
    public String getName() {
        return name;
    }

    public String getMajor() {
        return major;
    }

    public String getUniversity() {
        return university;
    }
}

public class Task2 {
    public static void main(String[] args) {
        // Membuat objek HashMap
        HashMap<Integer, Student> hashMap = new HashMap<>();

        // Menambahkan entri ke dalam HashMap
        Student Student1 = new Student("Dipo", "TI", "ITPLN");
        Student Student2 = new Student("Juju", "Seni", "Unbraw");
        Student Student3 = new Student("Dodo", "Fisika", "UGM");

        hashMap.put(1, Student1);
        hashMap.put(2, Student2);
        hashMap.put(3, Student3);

        System.out.print("Search Student by Name : ");
        try (Scanner keyboard = new Scanner(System.in)) {
            String studentName = keyboard.nextLine();
      
            try {
                int key = Integer.parseInt(studentName);
                if (hashMap.containsKey(key)) {
                    Student retrievedEntry = hashMap.get(key);
                    System.out.println("====Student Data====");
                    System.out.println("Name : " + retrievedEntry.getName());
                    System.out.println("Major: " + retrievedEntry.getMajor());
                    System.out.println("University: " + retrievedEntry.getUniversity());
                } else {
                    System.out.println("====Data Not Found=====");
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a valid integer key.");
            }
        }
    }
}
